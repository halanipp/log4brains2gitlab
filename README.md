# log4brains2gitlab

Container image for building and exporting a log4brains site to GitLab Pages.

## Usage

Use this image for your GitLab pipeline, e.g.:

```yml
pages:
  stage: deploy
  image: registry.gitlab.com/halanipp/log4brains2gitlab
  variables:
    GIT_DEPTH: 0
  script: log4brains2gitlab
  artifacts:
    paths:
      - public
```

This pipeline step will export the log4brains site to the `public/` directory, which GitLab Pages will then automatically publish.

**NOTE**: the pipeline step must specifically be named `pages` for GitLab Pages to work (see [docs](https://docs.gitlab.com/ee/ci/yaml/README.html#pages)).

### (Optional) Enable caching

Enabling npm artifact caching can improve build times for subsequent builds.

To do this, add the following to your pipeline:

```yml
cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - .npm/
```

### (Optional) Theming

The theme can be customised in the following ways:

- logo: upload your custom logo as `docs/theme/logo.png`.
