#!/bin/sh

# Prepare target dir
mkdir -p public/

# Build source ADRs and copy (with images) into target
npx log4brains build --basePath ${CI_PAGES_URL#*$CI_PAGES_DOMAIN} --out public/
find docs/adr -type f -regex '.*\.\(gif\|jpg\|png\)$' -exec cp {} public/adr \;

# Update theme
[ -f docs/theme/logo.png ] && cp -v docs/theme/logo.png public/l4b-static/Log4brains-logo-dark.png

# Optimise network performance
find public/ -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\)$' -exec gzip -f -k {} \;
